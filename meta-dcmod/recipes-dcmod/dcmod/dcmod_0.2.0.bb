DCMOD_BASE_DIR = "../../../../../../../../.."

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${DCMOD_BASE_DIR}/COPYING;md5=d32239bcb673463ab874e80d47fae504"

require recipes-python/python/python-set-ver.inc
inherit systemd

DCMOD_EGG = "${PN}-${PV}-py${PYTHON_BASEVERSION}.egg"
DCMOD_DATA_DIR = "${localstatedir}/lib/dcmod/data"

RDEPENDS_${PN} = " \
    libms5803 \
    smemlcd \
    python3-pyyaml \
    python3-decotengu \
    python3-pillow \
    python3-tornado \
    python3-flask \
"

FILES_${PN} = " \
    ${PYTHON_SITEPACKAGES_DIR}/${DCMOD_EGG} \
    ${PYTHON_SITEPACKAGES_DIR}/${PN}.pth \
    ${bindir}/* \
    ${sysconfdir}/dcmod \
    ${DCMOD_DATA_DIR} \
    ${datadir}/dcmod \
"

SYSTEMD_SERVICE_${PN} = "dcmod.service"

do_install() {
    # python modules and scripts
    install -d ${D}${PYTHON_SITEPACKAGES_DIR}/
    install ${DCMOD_BASE_DIR}/dist/${DCMOD_EGG} ${D}${PYTHON_SITEPACKAGES_DIR}/
    echo ${DCMOD_EGG} > ${D}${PYTHON_SITEPACKAGES_DIR}/${PN}.pth

    install -d ${D}${bindir}
    install ${DCMOD_BASE_DIR}/bin/dcmod ${D}${bindir}/

    # configuration
    install -d ${D}${sysconfdir}/dcmod/
    install ${DCMOD_BASE_DIR}/conf/dcmod.conf ${D}${sysconfdir}/dcmod/

    # systemd service
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${DCMOD_BASE_DIR}/service/dcmod.service ${D}${systemd_unitdir}/system

    # data log directory
    install -d ${D}${DCMOD_DATA_DIR}

    # fonts
    install -d ${D}${datadir}/dcmod/fonts
    install ${DCMOD_BASE_DIR}/fonts/*.pil ${D}${datadir}/dcmod/fonts
    install ${DCMOD_BASE_DIR}/fonts/*.pbm ${D}${datadir}/dcmod/fonts

    # remote console
    install -d ${D}${datadir}/dcmod/console/static
    install ${DCMOD_BASE_DIR}/dcmod/remote/static/*.html ${D}${datadir}/dcmod/console/static/
    install ${DCMOD_BASE_DIR}/dcmod/remote/static/*.js ${D}${datadir}/dcmod/console/static/
}
