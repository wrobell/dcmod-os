require at91bootstrap.inc

LIC_FILES_CHKSUM = "file://main.c;endline=27;md5=a2a70db58191379e2550cbed95449fbd"

COMPATIBLE_MACHINE = '(sama5d3xek|sama5d3-xplained|at91sam9x5ek|at91sam9rlek|at91sam9m10g45ek|sama5d4ek|sama5d4-xplained|sama5d2-xplained)'

SRC_URI = "https://github.com/linux4sam/at91bootstrap/archive/v${PV}.tar.gz;name=tarball"

SRC_URI[tarball.md5sum] = "4142a235d859cf5a5abbd1f4537264c9"
SRC_URI[tarball.sha256sum] = "dc07f9fb4d8fdcfaeb2cabe29b7a88ba6f14a7a3a3711aa879d06ae24403e866"


