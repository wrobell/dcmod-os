AT91BOOTSTRAP_CONFIG = "at91sam9x5eksd_linux_image"
AT91BOOTSTRAP_MACHINE = "at91sam9x5ek"
AT91BOOTSTRAP_LOAD = "sdcardboot"
COMPATIBLE_MACHINE = "arietta-g25"

do_configure_append() {
    echo CONFIG_IMAGE_NAME=\"zImage\" >> .config
    echo CONFIG_OF_LIBFDT=y >> .config
    echo CONFIG_OF_ADDRESS="0x21000000" >> .config
    echo CONFIG_LINUX_KERNEL_ARG_STRING="\"mem=128M console=ttyS0,115200 root=/dev/mmcblk0p2 rootdelay=2 spidev.bufsiz=12482\"" >> .config
}

do_deploy () {
    install -d ${DEPLOYDIR}
    install ${S}/binaries/${AT91BOOTSTRAP_BINARY} ${DEPLOYDIR}/${AT91BOOTSTRAP_IMAGE}

    cd ${DEPLOYDIR}
    ln -sf ${AT91BOOTSTRAP_IMAGE} ${MACHINE}-boot.bin
}

