LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

inherit kernel
require recipes-kernel/linux/linux-dtb.inc

LINUX_VERSION = "4.1"
LINUX_VERSION_EXTENSION = ".15"
S = "${WORKDIR}/linux-${LINUX_VERSION}${LINUX_VERSION_EXTENSION}"

SRC_URI = "https://www.kernel.org/pub/linux/kernel/v3.x/linux-${LINUX_VERSION}${LINUX_VERSION_EXTENSION}.tar.xz"
SRC_URI += "file://defconfig"
SRC_URI += "file://at91sam9g25ek.dts"
SRC_URI[md5sum] = "b227333912b161c96ff3e30f5041e1c0"
SRC_URI[sha256sum] = "472288cc966188c5d7c511c6be0f78682843c9ca2d5d6c4d67d77455680359a3"

# install Arietta G25 DTS file
do_configure_append() {
    install -m 0644 ${WORKDIR}/at91sam9g25ek.dts "${S}/arch/${ARCH}/boot/dts/"
}
