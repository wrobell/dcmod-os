#!/usr/bin/env python

from __future__ import print_function

import json
import sys


def manifest_lines(package, items, fmt_line, fmt_item):
    yield fmt_line(package, '', fmt_item(items[0]))
    if len(items) > 1:
        items = items[1:]
        items = (fmt_item(s) for s in items)
        items = (fmt_line(package, '+', s) for s in items)
        for s in items: # py3k: use "yield from items"
            yield s


def print_lines(package, items, fmt_line, fmt_item):
    items = sorted(items)
    lines = manifest_lines(package, items, fmt_line, fmt_item)
    print('\n'.join(lines))
    print()


def fmt_file(s):
    return s if s.startswith('${') \
        else '${{libdir}}/python${{PYTHON_BASEVERSION}}/{}'.format(s)


fmt_s = 'SUMMARY_${{PN}}-{} = "{}"'.format
fmt_d = 'RDEPENDS_${{PN}}-{} {}= "{}"'.format
fmt_f = 'FILES_${{PN}}-{} {}= "{}"'.format
fmt_p = 'PACKAGES{:.0} {}= "{}"'.format  # note: package name is ignored
fmt_package = '${{PN}}-{}'.format

data = json.load(sys.stdin)

print(
    '#\n# WARNING: This file is AUTO GENERATED: Manual edits will'
    ' be lost.\n#\n'
)

packages = sorted(list(data) + ['modules'])
packages = ('PROVIDES += "${{PN}}-{}"'.format(s) for s in packages)
print('\n'.join(packages))
print()

packages = list(data) + ['dbg', 'modules']
print_lines('', packages, fmt_p, fmt_package)

for package, info in sorted(data.items()):
    print(fmt_s(package, info['desc']))

    print_lines(package, info['deps'], fmt_d, fmt_package)
    print_lines(package, info['files'], fmt_f, fmt_file)

is_ok = lambda s: not (s in {'dev', 'distutils-staticdev'} or s.endswith('tests'))
deps = [s for s in data if is_ok(s)]
print(fmt_s('modules', 'All Python modules'))
print_lines('modules', deps, fmt_d, fmt_package)

print('\nALLOW_EMPTY_${PN}-modules = "1"')
