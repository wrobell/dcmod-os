SUMMARY = "Library for Sharp Memory LCDs"
HOMEPAGE = "https://github.com/wrobell/smemlcd"
SECTION = "libs"
LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = "python3-setuptools-native"

SRC_URI = "https://github.com/wrobell/${PN}/releases/download/${PN}-${PV}/${PN}-${PV}.tar.xz"
SRC_URI[md5sum] = "323ca7537ee5adc22b1a67615eb1db0e"
SRC_URI[sha256sum] = "57e0192925206d21946209c1fda784049f74ce4ca70abe354387f74e88db7c4d"

require recipes-python/python/python-dist-autotools.inc

EXTRA_OECONF_append = "BOARD=at91"
