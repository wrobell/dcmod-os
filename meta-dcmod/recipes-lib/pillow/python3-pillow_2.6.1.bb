SUMMARY = "Python Imaging Library (PIL)"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://docs/LICENSE;md5=f03075aa611d08bd0612dada2baba4cc"
SRCNAME = "Pillow"
PR = "r0"

SRC_URI = "https://pypi.python.org/packages/source/P/Pillow/${SRCNAME}-${PV}.tar.gz \
           file://010-set-paths.patch"

SRC_URI[md5sum] = "4b77fb0c81bbe0c8bf90c6eea960e091"
SRC_URI[sha256sum] = "f94342cf8aad63079b3ad8230e234060694a07a166a3a338421f4e3e0e338347"

DEPENDS = "zlib"

S = "${WORKDIR}/${SRCNAME}-${PV}"

require recipes-python/python/python-dist.inc

DISTUTILS_BUILD_EXT_ARGS="\
  --disable-freetype \
  --disable-jpeg \
  --disable-jpeg2000 \
  --disable-lcms \
  --disable-tcl \
  --disable-tiff \
  --disable-tk \
  --disable-webp \
  --disable-webpmux \
  --enable-zlib \
"

do_compile_prepend() {
    if [ ${BUILD_SYS} != ${HOST_SYS} ]; then
            SYS=${MACHINE}
    else
            SYS=${HOST_SYS}
    fi
    STAGING_INCDIR=${STAGING_INCDIR} \
    STAGING_LIBDIR=${STAGING_LIBDIR} \
    BUILD_SYS=${BUILD_SYS} HOST_SYS=${SYS} \
    ${STAGING_BINDIR_NATIVE}/${PYTHON_PN}-native/${PYTHON_PN} setup.py \
        build_ext ${DISTUTILS_BUILD_EXT_ARGS} || \
    bbfatal "${PYTHON_PN} setup.py build_ext execution failed."
}

# hack as setup.py install hangs
do_install() {
    install -d ${D}${PYTHON_SITEPACKAGES_DIR}
    cp -r build/lib.linux-x86_64-3.4/PIL ${D}${PYTHON_SITEPACKAGES_DIR}
}

RDEPENDS_${PN} += "python3-lang"
