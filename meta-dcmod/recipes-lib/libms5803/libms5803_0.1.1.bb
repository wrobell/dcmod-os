SUMMARY = "MS5803 pressure sensor C library with Python 3 bindings"
HOMEPAGE = "https://github.com/wrobell/libms5803"
SECTION = "libs"
LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"
PR = "r0"

DEPENDS = "libcheck python3-setuptools-native"

SRC_URI = "https://github.com/wrobell/${PN}/releases/download/${PN}-${PV}/libms5803-${PV}.tar.xz"
SRC_URI[md5sum] = "3379b54cd3ef3bf6fdba0d0fe2027492"
SRC_URI[sha256sum] = "5b52f00671a8589cc3f1c146e1ffa46d3ff490cb0339d7323b8210a3649e23ee"

require recipes-python/python/python-dist-autotools.inc
