SUMMARY = "Python web framework and asynchronous networking library"
HOMEPAGE = "http://www.tornadoweb.org/"
SECTION = "libs"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://README.rst;md5=a824240a9c58ab2ce05c6f24d238df6c"

DEPENDS = "python3-setuptools-native"

SRC_URI = "https://pypi.python.org/packages/source/t/tornado/tornado-${PV}.tar.gz"
SRC_URI[md5sum] = "d13a99dc0b60ba69f5f8ec1235e5b232"
SRC_URI[sha256sum] = "c9c2d32593d16eedf2cec1b6a41893626a2649b40b21ca9c4cac4243bde2efbf"

S = "${WORKDIR}/tornado-${PV}"

require recipes-python/python/python-dist.inc
