SUMMARY = "Dive decompression library"
HOMEPAGE = "http://dcmod.org/decotengu"
SECTION = "libs"
LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = "python3-setuptools-native"

SRC_URI = "https://pypi.python.org/packages/source/d/decotengu/decotengu-${PV}.tar.bz2"
SRC_URI[md5sum] = "874ef4125edf310dd93f3ac627ba67e7"
SRC_URI[sha256sum] = "a836b26f940703c07d453cdc73abee066858c22017e693a035334c0f4cde9cce"

S = "${WORKDIR}/decotengu-${PV}"

require recipes-python/python/python-dist.inc

do_configure_prepend() {
    rm -f Makefile
}
