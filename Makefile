SHELL = /bin/bash

YOCTO_VER = 2.0
DCMOD_VER = 0.2.0

MACHINE ?= arietta-g25

YOCTO_NAME = poky-jethro-14.0.0
YOCTO_DIR = $(YOCTO_NAME)

.os-fetch-stamp: $(YOCTO_NAME)-python.patch
	wget -c http://downloads.yoctoproject.org/releases/yocto/yocto-$(YOCTO_VER)/$(YOCTO_NAME).tar.bz2
	tar xjf $(YOCTO_NAME).tar.bz2
	ln -sf /usr/bin/python2 $(YOCTO_DIR)/bitbake/bin/python
	# some hacks due to broken python support in yocto
	patch -p1 -d $(YOCTO_DIR) < $(YOCTO_NAME)-python.patch
	touch .os-fetch-stamp

.os-atmel-stamp: .os-fetch-stamp
	cd $(YOCTO_DIR); git clone http://github.com/linux4sam/meta-atmel
	rm -rf $(YOCTO_DIR)/meta-atmel/recipes-qt/
	rm -rf $(YOCTO_DIR)/meta-atmel/recipes-bsp/u-boot/
	touch .os-atmel-stamp

.os-dcmod-layer-stamp: .os-atmel-stamp
	cp -uav meta-dcmod $(YOCTO_DIR)
	touch .os-dcmod-layer-stamp

.os-dcmod-init-stamp: .os-dcmod-layer-stamp
	cd $(YOCTO_DIR); \
	PATH=$$(pwd)/bitbake/bin:$$PATH \
		TEMPLATECONF=meta-dcmod/conf \
		source oe-init-build-env build-dcmod
	touch .os-dcmod-init-stamp

all: .os-dcmod-init-stamp
	cd $(YOCTO_DIR)/build-dcmod; \
	PATH=$$(pwd)/../bitbake/bin:$$PATH \
		MACHINE=$(MACHINE) BB_ENV_EXTRAWHITE=MACHINE \
		bitbake core-image-minimal
	mkdir -p dist
	ln -sf ../$(YOCTO_DIR)/build-dcmod/tmp/deploy/images dist

os-dcmod:
	cd $(YOCTO_DIR)/build-dcmod; \
	PATH=$$(pwd)/../bitbake/bin:$$PATH \
		MACHINE=$(MACHINE) BB_ENV_EXTRAWHITE=MACHINE \
		bitbake dcmod

clean-os:
	rm -rf .os-*-stamp

clean-os-dcmod:
	cd $(YOCTO_DIR)/build-dcmod; \
	PATH=$$(pwd)/../bitbake/bin:$$PATH \
		MACHINE=$(MACHINE) BB_ENV_EXTRAWHITE=MACHINE \
		bitbake dcmod -f -c cleanall

